import Iconify from '../../components/Iconify'

export const account = {
  displayName: 'Suulola Oluwaseyi',
  email: 'oluwaseyisuulola@gmail.com',
  photoURL: `https://visualpharm.com/assets/30/User-595b40b85ba036ed117da56f.svg`
}

const getIcon = (name: any) => <Iconify icon={name} width={22} height={22} />

export const navConfig = [
  {
    title: 'dashboard',
    path: '/',
    icon: getIcon('eva:pie-chart-2-fill')
  },
  {
    title: 'users',
    path: '/',
    icon: getIcon('eva:people-fill')
  },
  {
    title: 'blog',
    path: '/',
    icon: getIcon('eva:file-text-fill')
  },
  {
    title: 'transactions',
    path: '/',
    icon: getIcon('eva:shopping-bag-fill')
  },
  {
    title: 'analysis',
    path: '/',
    icon: getIcon('eva:lock-fill')
  },
  {
    title: 'reports',
    path: '/',
    icon: getIcon('eva:file-text-fill')
  },
  {
    title: 'investment',
    path: '/',
    icon: getIcon('eva:pie-chart-2-fill')
  },
  {
    title: 'settings',
    path: '/',
    icon: getIcon('eva:lock-fill')
  }
]
