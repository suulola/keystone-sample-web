import { styled } from '@mui/material/styles'
import { Box, Drawer, Typography, Avatar, MenuItem } from '@mui/material'
import useResponsive from '../../hooks/useResponsive'
import Scrollbar from '../../../components/Scrollbar'
import { account, navConfig } from '../../../util/data'
import NavSection from '../../../components/NavSection'

const DRAWER_WIDTH = 280

const RootStyle = styled('div')(({ theme }) => ({
  [theme.breakpoints.up('lg')]: {
    flexShrink: 0,
    width: DRAWER_WIDTH
  }
}))

const AccountStyle = styled('div')(({ theme }: any) => ({
  display: 'flex',
  alignItems: 'center',
  padding: theme.spacing(2, 2.5),
  borderRadius: Number(theme.shape.borderRadius) * 1.5,
  backgroundColor: theme.palette.grey[500_12]
}))

const Sidebar = ({ isOpenSidebar, onCloseSidebar }: any) => {
  const isDesktop = useResponsive('up', 'lg')

  const renderContent = (
    <Scrollbar
      sx={{
        height: 1,
        '& .simplebar-content': {
          height: 1,
          display: 'flex',
          flexDirection: 'column'
        }
      }}
    >
      <Box sx={{ px: 2.5, py: 3, display: 'inline-flex' }}>
        <Typography variant='subtitle2' sx={{ color: 'text.primary' }}>
          Retro
        </Typography>
      </Box>

      <Box sx={{ mb: 5, mx: 2.5 }}>
        <MenuItem onClick={onCloseSidebar}>
          <AccountStyle>
            <Avatar src={account.photoURL} alt='photoURL' />
            <Box sx={{ ml: 2 }}>
              <Typography variant='subtitle2' sx={{ color: 'text.primary' }}>
                {account.displayName}
              </Typography>
            </Box>
          </AccountStyle>
        </MenuItem>
      </Box>

      <NavSection navConfig={navConfig} />

      <Box sx={{ flexGrow: 1 }} />
    </Scrollbar>
  )

  return (
    <RootStyle>
      {!isDesktop && (
        <Drawer
          open={isOpenSidebar}
          onClose={onCloseSidebar}
          PaperProps={{
            sx: { width: DRAWER_WIDTH }
          }}
        >
          {renderContent}
        </Drawer>
      )}

      {isDesktop && (
        <Drawer
          open
          variant='persistent'
          PaperProps={{
            sx: {
              width: DRAWER_WIDTH,
              bgcolor: 'background.default',
              borderRightStyle: 'dashed'
            }
          }}
        >
          {renderContent}
        </Drawer>
      )}
    </RootStyle>
  )
}

export default Sidebar
