import { Box, Stack, Avatar, Card, Typography, CardHeader } from '@mui/material'
import Scrollbar from '../Scrollbar'

const TableItem = ({ news }: any) => {
  const { image } = news

  return (
    <Stack direction='row' alignItems='center' spacing={2}>
      <Avatar
        alt={`Table Image`}
        src={image}
        sx={{ width: 48, borderRadius: 1.5, flexShrink: 0 }}
      />

      <Box sx={{ flexGrow: 1 }}>
        <Typography color='inherit' variant='subtitle2' noWrap>
          {`Sample Table Title`}
        </Typography>

        <Typography variant='body2' sx={{ color: 'text.secondary' }} noWrap>
          {`Sample Table SubHeading`}
        </Typography>
      </Box>

      <Typography
        variant='caption'
        sx={{ pr: 3, flexShrink: 0, color: 'text.secondary' }}
      >
        Published
      </Typography>
    </Stack>
  )
}

const Table = ({ title, list }: any) => {
  return (
    <Card>
      <CardHeader subheader={title} />

      <Scrollbar>
        <Stack spacing={3} sx={{ p: 3, pr: 0 }}>
          {list.map((news: any, index: number) => (
            <TableItem key={index} news={news} />
          ))}
        </Stack>
      </Scrollbar>
    </Card>
  )
}

export default Table
