import { Card, Typography, Box } from '@mui/material'
import Iconify from '../Iconify'


export default function AppWidgetSummary ({
  title,
  total,
  icon,
  color = 'primary',
  ...other
}: any) {
  return (
    <Card
      sx={{
        py: 5,
        pl: 5,
        pr: 2,
        alignItems: 'center',
        boxShadow: 0,
        textAlign: 'center',
        color: (theme: any) => theme.palette[color].darker,
        bgcolor: (theme: any) => theme.palette[color].lighter,
        display: 'flex',
        justifyContent: 'space-between'
      }}
      {...other}
    >
      <Box>
        <Typography variant='subtitle2' sx={{ opacity: 0.72 }}>
          {title}
        </Typography>
        <Typography variant='h3'>${total}</Typography>
      </Box>
      <Iconify icon={icon} width={40} height={60} />
    </Card>
  )
}
