import merge from 'lodash/merge'
import dynamic from 'next/dynamic'
import { Card, CardHeader, Button, Grid, Divider } from '@mui/material'
import BaseOptionChart from './BaseOptionChart'
import AddIcon from '@mui/icons-material/Add'
const ReactApexChart = dynamic(() => import('react-apexcharts'), { ssr: false })

const SalesOverview = ({ title, chartLabels, chartData, ...other }: any) => {
  const chartOptions: any = merge(BaseOptionChart(), {
    plotOptions: { bar: { columnWidth: '16%' } },
    fill: { type: chartData.map((i: any) => i.fill) },
    labels: chartLabels,
    xaxis: { type: 'datetime' },
    tooltip: {
      shared: true,
      intersect: false,
      y: {
        formatter: (y: any) => {
          if (typeof y !== 'undefined') {
            return `${y.toFixed(0)} visits`
          }
          return y
        }
      }
    }
  })

  return (
    <Card {...other}>
      <Grid
        sx={{
          display: 'flex',
          justifyContent: 'space-between'
        }}
      >
        <CardHeader title={title} />
        <Button
          startIcon={<AddIcon />}
          sx={{
            height: theme => theme.spacing(5),
            mt: 3,
            mr: 5,
            px: 4
          }}
          variant='contained'
          size='small'
        >
          Add Offer
        </Button>
      </Grid>

      <Divider
        sx={{
          my: 3
        }}
      />

      <Grid sx={{ p: 3, pb: 1 }} dir='ltr'>
        <ReactApexChart
          type='line'
          series={chartData}
          options={chartOptions}
          height={364}
        />
      </Grid>
    </Card>
  )
}

export default SalesOverview
