import { useRef, useState } from 'react'
import { alpha } from '@mui/material/styles'
import {
  Box,
  Divider,
  Typography,
  Stack,
  MenuItem,
  Avatar,
  IconButton
} from '@mui/material'
import MenuPopover from './MenuPopover'
import { account } from '../util/data'

const MENU_OPTIONS = [
  {
    label: 'Home',
    icon: 'eva:home-fill',
    linkTo: '/'
  },
  {
    label: 'Profile',
    icon: 'eva:person-fill',
    linkTo: '/'
  },
  {
    label: 'Settings',
    icon: 'eva:settings-2-fill',
    linkTo: '/'
  }
]

const AccountPopover = () => {
  const anchorRef = useRef(null)

  const [open, setOpen] = useState(null)

  const handleOpen = (event: any) => {
    setOpen(event.currentTarget)
  }

  const handleClose = () => {
    setOpen(null)
  }

  let abc = open
    ? {
        '&:before': {
          zIndex: 1,
          content: "''",
          width: '100%',
          height: '100%',
          borderRadius: '50%',
          position: 'absolute',
          bgcolor: (theme: any) => alpha(theme.palette.grey[900], 0.8)
        }
      }
    : {}

  return (
    <>
      <IconButton
        ref={anchorRef}
        onClick={handleOpen}
        sx={{
          p: 0,
          ...abc
        }}
      >
        <Avatar src={`https://visualpharm.com/assets/30/User-595b40b85ba036ed117da56f.svg`} alt='photoURL'   />
      </IconButton>

      <MenuPopover
        open={Boolean(open)}
        anchorEl={open}
        onClose={handleClose}
        sx={{
          p: 0,
          mt: 1.5,
          ml: 0.75,
          '& .MuiMenuItem-root': {
            typography: 'body2',
            borderRadius: 0.75
          }
        }}
      >
        <Box sx={{ my: 1.5, px: 2.5 }}>
          <Typography variant='subtitle2' noWrap>
            {account.displayName}
          </Typography>
          <Typography variant='body2' sx={{ color: 'text.secondary' }} noWrap>
            {account.email}
          </Typography>
        </Box>

        <Divider sx={{ borderStyle: 'dashed' }} />

        <Stack sx={{ p: 1 }}>
          {MENU_OPTIONS.map((option, i) => (
            <MenuItem key={i} onClick={handleClose}>
              {option.label}
            </MenuItem>
          ))}
        </Stack>
      </MenuPopover>
    </>
  )
}

export default AccountPopover
