import { alpha, useTheme, styled } from '@mui/material/styles'
import {
  Box,
  List,
  ListItemText,
  ListItemIcon,
  ListItemButton
} from '@mui/material'

const ListItemStyle = styled((props: any) => (
  <ListItemButton disableGutters {...props} />
))(({ theme }: any) => ({
  ...theme.typography.body2,
  height: 48,
  position: 'relative',
  textTransform: 'capitalize',
  color: theme.palette.text.secondary,
  borderRadius: theme.shape.borderRadius
}))

const ListItemIconStyle = styled(ListItemIcon)({
  width: 22,
  height: 22,
  color: 'inherit',
  display: 'flex',
  alignItems: 'center',
  justifyContent: 'center'
})

interface INavItem {
  item: {
    title: string
    path: string
    icon: string
  }
}

function NavItem ({ item }: INavItem) {
  const theme = useTheme()

  const { title, path, icon } = item

  return (
    <ListItemStyle
      to={path}
      sx={{
        color: 'primary.main',
        marginTop: 1,
        bgcolor: alpha(
          theme.palette.primary.main,
          theme.palette.action.selectedOpacity
        )
      }}
    >
      <ListItemIconStyle>{icon && icon}</ListItemIconStyle>
      <ListItemText disableTypography primary={title} />
    </ListItemStyle>
  )
}

interface INavSection {
  navConfig: any[]
}

const NavSection = ({ navConfig, ...other }: INavSection) => {
  return (
    <Box {...other}>
      <List disablePadding sx={{ p: 1 }}>
        {navConfig.map(item => (
          <NavItem key={item.title} item={item} />
        ))}
      </List>
    </Box>
  )
}

export default NavSection
