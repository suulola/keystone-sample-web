import { Icon } from '@iconify/react'
import { Box } from '@mui/material'

interface IIcon {
  icon: string
  sx?: any
  width?: number
  height?: number
}

const Iconify = ({ icon, sx, ...other }: IIcon) => {
  return <Box component={Icon} icon={icon} sx={{ ...sx }} {...other} />
}

export default Iconify
