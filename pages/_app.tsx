import '../styles/globals.css'
import { AppProps } from 'next/app'
import Head from 'next/head'

function MyApp ({ Component, pageProps }: AppProps) {
  return (
    <>
      <Head>
        <link
          href='https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap'
        />
        <link
          href='https://fonts.googleapis.com/icon?family=Material+Icons'
        />
        <link rel='icon' href='/favicon.ico' />
        <title>Keystone Assessment</title>
        <meta name='Keystone' content='Keystone Assessment' />
        <meta name='viewport' content='initial-scale=1, width=device-width' />
      </Head>
      <Component {...pageProps} />
    </>
  )
}

export default MyApp
