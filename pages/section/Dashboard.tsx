import { faker } from '@faker-js/faker'
import { useTheme } from '@mui/material/styles'
import { Grid, Container } from '@mui/material'
import {
  Table,
  AppWidgetSummary,
  SalesOverview,
  RadarChat
} from '../../components/chart'

const Dashboard = () => {
  const theme = useTheme()

  return (
    <>
      <Container maxWidth='xl'>
        <Grid mb={5} mt={0} item xs={12} md={12} lg={12}>
          <SalesOverview
            title='Sales Overview'
            chartLabels={[
              '01/01/2003',
              '02/01/2003',
              '03/01/2003',
              '04/01/2003',
              '05/01/2003',
              '06/01/2003',
              '07/01/2003',
              '08/01/2003',
              '09/01/2003',
              '10/01/2003',
              '11/01/2003'
            ]}
            chartData={[
              {
                name: 'Income',
                type: 'area',
                fill: 'gradient',
                data: [44, 55, 41, 67, 22, 43, 21, 41, 56, 27, 43]
              },
              {
                name: 'Expenses',
                type: 'area',
                fill: 'gradient',
                data: [30, 25, 36, 30, 45, 35, 64, 52, 59, 36, 39]
              }
            ]}
          />
        </Grid>

        <Grid container spacing={3}>
          <Grid item xs={12} sm={6} md={3}>
            <AppWidgetSummary
              title='Daily Income'
              total={345}
              icon={'ant-design:area-chart-outlined'}
            />
          </Grid>

          <Grid item xs={12} sm={6} md={3}>
            <AppWidgetSummary
              title='Daily Expenses'
              total={380}
              color='info'
              icon={'ant-design:bar-chart-outlined'}
            />
          </Grid>

          <Grid item xs={12} sm={6} md={3}>
            <AppWidgetSummary
              title='Weekly Income'
              total={5380}
              color='warning'
              icon={'ant-design:line-chart-outlined'}
            />
          </Grid>

          <Grid item xs={12} sm={6} md={3}>
            <AppWidgetSummary
              title='Weekly Expenses'
              total={4320}
              color='error'
              icon={'ant-design:bar-chart-outlined'}
            />
          </Grid>

          <Grid item xs={12} md={6} lg={4}>
            <RadarChat
              title='Top Selling Categories'
              chartLabels={[
                'Sho2',
                'Jeans',
                'Outwear',
                'T-Shirts',
                'Accessories'
              ]}
              chartData={[
                { name: 'Series 1', data: [80, 50, 30, 40, 100] },
                { name: 'Series 2', data: [20, 30, 40, 80, 20] },
                { name: 'Series 3', data: [44, 76, 78, 13, 43] }
              ]}
              chartColors={[...Array(5)].map(
                () => theme.palette.text.secondary
              )}
            />
          </Grid>

          <Grid item xs={12} md={6} lg={8}>
            <Table
              title='Latest Added Product'
              list={[...Array(5)].map((_, index) => ({
                id: faker.datatype.uuid(),
                title: faker.name.jobTitle(),
                description: faker.name.jobTitle(),
                image: `https://visualpharm.com/assets/30/User-595b40b85ba036ed117da56f.svg`,
                postedAt: faker.date.recent()
              }))}
            />
          </Grid>
        </Grid>
      </Container>
    </>
  )
}

export default Dashboard
