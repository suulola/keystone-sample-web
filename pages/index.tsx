import { NextPage } from 'next'
import ThemeProvider from '../theme'
import DashboardLayout from '../layout/DashboardLayout'

const Home: NextPage = () => {
  return (
    <ThemeProvider>
      <DashboardLayout />
    </ThemeProvider>
  )
}

export default Home
